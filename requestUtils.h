#ifndef _REQUESTUTILS_H_
#define _REQUESTUTILS_H_

#include <vector>
#include <unordered_map>
#include <string>

#define BUFFER_SIZE 1024
#define ERR_404 "HTTP/1.1 404 Not Found\r\n\r\n"

typedef struct HttpReq
{
	char method[BUFFER_SIZE];
	char path[BUFFER_SIZE];
	char version[BUFFER_SIZE];
	std::string header;
	std::string body;
	std::vector<char*> pathTok;
	std::unordered_map<std::string, std::string> params;
} HttpReq;

std::vector<char*> parsePath(char* path);
HttpReq parseReq(char* request);
std::unordered_map<std::string, std::string> getParams(const std::string& request);
std::string toAscii(const std::string& str);

#endif //_REQUESTUTILS_H_
