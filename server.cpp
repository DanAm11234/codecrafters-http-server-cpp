#include <iostream>
#include <cstdlib>
#include <string>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <algorithm>
#include <memory>
#include <vector>
#include <sstream>
#include <thread>
#include <fstream>

#include "requestUtils.h"

typedef struct sockaddr_in sockaddr_in;

void handleClient(int client_fd, const std::string& baseDir)
{
	char buffer[BUFFER_SIZE];
	size_t contentLen = 0;
	ssize_t status = read(client_fd, buffer, BUFFER_SIZE);

	if (status < 0)
	{
		close(client_fd);
		return;
	}

	HttpReq request = parseReq(buffer);

	char response[BUFFER_SIZE] = "";
//	char response[BUFFER_SIZE] = "HTTP/1.1 200 OK\r\n\r\n";
	if (0 == std::strcmp(request.method, "POST"))
	{
		if (0 == std::strcmp(request.pathTok.at(0), "files"))
		{
			std::string filePath = baseDir + request.pathTok.at(1);
			std::ofstream file(filePath);

			if (file.is_open())
			{
				file << toAscii(request.body);
				std::strcpy(response, "HTTP/1.1 201 OK\r\n\r\n");
				file.close();
			}
		}
	}
	else if (0 == std::strcmp(request.method, "GET"))
	{
		if (0 == std::strcmp(request.path, "/"))
			std::strcpy(response, "HTTP/1.1 200 OK\r\n\r\n");
		else if (0 == std::strcmp(request.pathTok.at(0), "echo"))
		{
			contentLen = std::strlen(request.path + 6);
			std::sprintf(response, "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\nContent-Length: %zu\r\n\r\n%s\r\n", contentLen,
				request.path + 6);
		}
		else if (0 == std::strcmp(request.pathTok.at(0), "user-agent"))
		{
			std::string resBody = request.params["User-Agent"];
			contentLen = resBody.length();

			std::sprintf(response, "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\nContent-Length: %zu\r\n\r\n%s\r\n",
				contentLen,
				resBody.c_str());
		}
		else if (0 == std::strcmp(request.pathTok.at(0), "files"))
		{
			std::string filepath = baseDir + request.pathTok.at(1);
			std::ifstream file(filepath);

			if (file.is_open())
			{
				std::string line;
				std::string resBody;

				while (getline(file, line)) resBody.append(line);

				std::sprintf(response,
					"HTTP/1.1 200 OK\r\nContent-Type: application/octet-stream\r\nContent-Length: %zu\r\n\r\n%s\r\n",
					resBody.length(),
					resBody.c_str());

				file.close();
			}
			else std::strcpy(response, ERR_404);
		}
		else std::strcpy(response, ERR_404);
	}

	send(client_fd, response, std::strlen(response), 0);
	close(client_fd);
}

int main(int argc, char** argv)
{
	// You can use print statements as follows for debugging, they'll be visible when running tests.
	std::cout << "Logs from your program will appear here!\n";

	int server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (server_fd < 0)
	{
		std::cerr << "Failed to create server socket\n";
		return 1;
	}

	// Since the tester restarts your program quite often, setting REUSE_PORT
	// ensures that we don't run into 'Address already in use' errors
	int reuse = 1;
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEPORT, &reuse, sizeof(reuse))
		< 0)
	{
		std::cerr << "setsockopt failed\n";
		return 1;
	}

	struct sockaddr_in server_addr;
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port = htons(4221);

	if (bind(server_fd, (struct sockaddr*)&server_addr, sizeof(server_addr))
		!= 0)
	{
		std::cerr << "Failed to bind to port 4221\n";
		return 1;
	}

	std::string BASE_DIR;

	if (argc > 1)
	{
		if (0 == std::strcmp(argv[1], "--directory"))
			BASE_DIR = std::string(argv[2]);
	}

	int connection_backlog = 5;
	if (listen(server_fd, connection_backlog) != 0)
	{
		std::cerr << "listen failed\n";
		return 1;
	}

	std::cout << "Waiting for a client to connect...\n";

	int killCounter = 0;
	std::vector<std::thread> clients;

	while (true)
	{
		if (killCounter >= 10) break;

		struct sockaddr_in client_addr;
		int client_addr_len = sizeof(client_addr);
		int client_fd = accept(server_fd, (struct sockaddr*)&client_addr, (socklen_t*)&client_addr_len);

		if (client_fd < 0)
		{
			std::cout << "Connection failed\n";
			killCounter++;
			continue;
		}
		std::cout << "Client connected\n";

//		char msg[] = "HTTP/1.1 500 Not OK\r\n";
//		send(client_fd, msg, std::strlen(msg), 0);
		clients.emplace_back(handleClient, client_fd, BASE_DIR);
	}

	close(server_fd);

	return 0;
}
