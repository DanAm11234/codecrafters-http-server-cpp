#include <cstring>
#include <regex>
#include "requestUtils.h"
#include "requestUtils.h"

std::vector<char*> parsePath(char* path)
{
	char* cpy = (char*)malloc(std::strlen(path));
	std::strcpy(cpy, path);
	std::vector<char*> ret = std::vector<char*>();
	char* token = std::strtok(cpy, "/");
	ret.push_back(token);

	while ((token = std::strtok(nullptr, "/"))) ret.push_back(token);

	return ret;

}

HttpReq parseReq(char* request)
{
	HttpReq parsed;
	std::strcpy(parsed.method, std::strtok(request, " "));
	std::strcpy(parsed.path, std::strtok(nullptr, " "));
	char* version = std::strtok(nullptr, "\n");
	std::strcpy(parsed.version, version);
	parsed.pathTok = parsePath(parsed.path);
//	parsed.header = std::string(std::strrchr(request, '\0'));
	parsed.header = std::string(version + std::strlen(version) + 1);
//	std::strcpy(parsed.header, std::strlen(parsed.version) + parsed.version);
	parsed.params = getParams(parsed.header);

	if (parsed.params.contains("body"))
	{
		parsed.body = parsed.params.at("body");
		parsed.params.extract("body");
	}

	return parsed;
}

std::string trim(std::string s)
{
	std::regex e("^\\s+|\\s+$"); // remove leading and trailing spaces
	return std::regex_replace(s, e, "");
}

std::unordered_map<std::string, std::string>
getParams(const std::string& request)
{
//	std::string copy = std::string(request);
	std::unordered_map<std::string, std::string> ret;

	size_t lastPos = 0;
	size_t pos = 0;

	while ((pos = request.find('\n', lastPos)) != std::string::npos)
	{
		std::string param = request.substr(lastPos, pos - lastPos);
		size_t split = param.find(':');

		if (split == std::string::npos)
		{
			ret.insert({ std::string("body"), request.substr(pos + 1) });
			break;
		}

		ret.insert({ trim(param.substr(0, split)),
					 trim(param.substr(split + 1)) });
		lastPos = pos + 1;
	}

	return ret;
}

std::string toAscii(const std::string& str)
{
	std::string ret;

	for (u_char character : str)
		if (isprint(character)) ret.push_back((char)character);

	return ret;
}
